﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exchange.Models;

namespace Exchange.Services
{
    /// <summary>
    /// interface for generic operations
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// To create a new exchange rate
        /// </summary>
        /// <param name="rate"></param>
        void Create(CurrencyExchangeRate rate);

        List<CurrencyExchangeRate> Get();

        
    }
}
