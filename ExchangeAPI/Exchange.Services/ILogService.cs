﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exchange.Services
{
    public interface ILogService
    {
        void LogException(Exception exception);
    }
}
