﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exchange.Services.Extensions
{
    public static class ExchangeExtensions
    {
        public static IEnumerable<string[]> GetExclusions(this ExchangeService exchangeService)
        {
           List<string> CurrencyList = new List<string> { "AUD", "SEK", "USD", "GBP", "EUR" };
           var excludes = from l1 in CurrencyList from l2 in CurrencyList select new[] {l1,l2 };
            return excludes;
        }
    }
}
