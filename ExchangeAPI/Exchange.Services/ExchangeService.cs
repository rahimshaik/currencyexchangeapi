﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Exchange.Models;
using Newtonsoft.Json;
using Exchange.Services.Exceptions;
using Exchange.Services.Extensions;
using Exchange.Repositories;
namespace Exchange.Services
{
    public class ExchangeService : IExchangeService
    {
        private readonly IExchangeRepository _repository;

        public ExchangeService(IExchangeRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<CurrencyExchangeRate> GetExchangeRate(string baseCurrency, string targetCurrency)
        {
            if (string.IsNullOrWhiteSpace(baseCurrency))
            {
                throw new ValidationException("VAL-001", "BaseCurrency is required.");
            }
            if (string.IsNullOrWhiteSpace(targetCurrency))
            {
                throw new ValidationException("VAL-002", "TargetCurrency is required.");
            }
            int length = baseCurrency.Length;
            if (length != 3)
            {
                throw new ValidationException("VAL-003", "Invalid BaseCurrency format.");
            }
            length = targetCurrency.Length;
            if (length != 3)
            {
                throw new ValidationException("VAL-004", "Invalid TargetCurrency format.");
            }

            return await _repository.GetCurrencyExchangeRate(baseCurrency, targetCurrency);
        }

       

        public void Create(CurrencyExchangeRate rate)
        {
            throw new NotImplementedException();
        }

        public List<CurrencyExchangeRate> Get()
        {
            throw new NotImplementedException();
        }

        
        

        


    }
}
