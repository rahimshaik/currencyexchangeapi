﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exchange.Services.Exceptions
{
    public class BaseException: Exception
    {
        public string ErrorCode { get; set; }

        public BaseException(string errorCode, string message) : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
