﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exchange.Services.Exceptions
{
    public class BusinessRuleException: BaseException
    {
        public BusinessRuleException(string errorCode, string message):base(errorCode, message)
        {

        }
    }
}
