﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exchange.Services.Exceptions
{
    public class ValidationException: BaseException
    {
        public ValidationException(string errorCode, string message) : base(errorCode, message)
        {

        }
    }
}
