﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exchange.Models;


namespace Exchange.Services
{
    public interface IExchangeService: IService
    {
        Task<CurrencyExchangeRate> GetExchangeRate(string baseCurrency, string targetCurrency);
    }
}
