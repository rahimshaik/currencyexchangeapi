﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exchange.Models;
namespace Exchange.Repositories
{
    /// <summary>
    /// Generic repository for generic operations
    /// </summary>
    public interface IRepository
    {
        void Create(CurrencyExchangeRate currencyExchangeRate);
        List<CurrencyExchangeRate> Get();
    }
}
