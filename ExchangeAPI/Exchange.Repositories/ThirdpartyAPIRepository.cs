﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Exchange.Models;

namespace Exchange.Repositories
{
    public class ThirdpartyAPIRepository : IExchangeRepository
    {
        public void Create(CurrencyExchangeRate currencyExchangeRate)
        {
            throw new NotImplementedException();
        }

        public List<CurrencyExchangeRate> Get()
        {
            throw new NotImplementedException();
        }

        public async Task<CurrencyExchangeRate> GetCurrencyExchangeRate(string from, string to)
        {
            CurrencyExchangeRate rate = null;
            var url = $"{ConfigReader.APIUrl}/latest?access_key={ConfigReader.APIKey}&base=EUR&symbols={to}";
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var responseMessage = await client.GetAsync(url);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var result = await responseMessage.Content.ReadAsStringAsync();
                    
                    var rateResult = JsonConvert.DeserializeObject<ExchangeRate>(result);
                    if (rateResult != null)
                    {
                        var targetCurrencyInfo = rateResult.Rates.Where(r => r.Key.ToLower() == to.ToLower()).FirstOrDefault();
                        var exchangeRate = targetCurrencyInfo.Value;

                        rate = new CurrencyExchangeRate() { BaseCurrency = from, TargetCurrency = to, ExchangeRate = exchangeRate, TimeStamp = DateTime.Parse(rateResult.date) };
                    }
                }
            }

            return rate;
        }
    }
}
