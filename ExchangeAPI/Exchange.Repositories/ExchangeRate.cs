﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Exchange.Repositories
{
    public class ExchangeRate
    {
        public bool success { get; set; }
        public double timestamp { get; set; }
        [JsonProperty("base")]
        public string basecurrency { get; set; }
        public string date { get; set; }
        public Dictionary<string, decimal> Rates { get; set; }
        
    }
}
