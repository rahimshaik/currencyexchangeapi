﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exchange.Models;
namespace Exchange.Repositories
{
    public interface IExchangeRepository: IRepository
    {
        Task<CurrencyExchangeRate> GetCurrencyExchangeRate(string from, string to);
    }
}
