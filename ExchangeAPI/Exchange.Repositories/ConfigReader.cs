﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Exchange.Repositories
{
    public static class ConfigReader
    {
        public static string APIUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["APIUrl"];
            }
        }

        public static string APIKey
        {
            get
            {
                return ConfigurationManager.AppSettings["APIKey"];
            }
        }
    }
}


