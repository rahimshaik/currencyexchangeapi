﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Exchange.Services;
using Exchange.Services.Exceptions;
using System.Threading.Tasks;
namespace ExchangeAPI.Controllers
{
    [RoutePrefix("api/exchange")]
    public class ExchangeController : ApiController
    {
        private readonly IExchangeService _service;
        private readonly ILogService _logService;


        public ExchangeController(IExchangeService service, ILogService logService)
        {
            this._service = service;
            this._logService = logService;
        }

        // GET: api/Exchange/5
        [HttpGet]
        [Route("~/api/exchange/getrate/{baseCurrency}/{targetCurrency}")]
        public async Task<HttpResponseMessage> GetExachangeRate(string baseCurrency, string targetCurrency)
        {
            if (string.IsNullOrWhiteSpace(baseCurrency))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "BaseCurrency is required.");
            }

            if (string.IsNullOrWhiteSpace(targetCurrency))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TargetCurrency is required.");

            }
            try
            {
                var result = await _service.GetExchangeRate(baseCurrency, targetCurrency);
                if (result == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"No results found for BaseCurrency: {baseCurrency} and TargetCurrency: {targetCurrency}");
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { BaseCurrency = result.BaseCurrency, ToCurrency = result.TargetCurrency, ExchangeRate = result.ExchangeRate, result.TimeStamp });
            }
            catch(ValidationException validationException)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, validationException.Message);
            }
            catch(BusinessRuleException businessRuleException)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new {ErrorCode = businessRuleException.ErrorCode, ErrorMessage = businessRuleException.Message });
            }
            catch(Exception exception)
            {
                this._logService.LogException(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Some system error occured. Please try again.");
                
            }
            

            
        }

    }
}
